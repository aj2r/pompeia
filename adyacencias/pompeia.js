function pompeiaPunto(x,y){
		
		this.x = x;
		this.y = y;
		this.mostrarPunto = function(){
			return "("+this.x+", "+this.y+")";
		}
	}
	
	
	/***********************
	*Recta que pasa por dos puntos
	* Ax+By+C=0
	************************/
	function pompeiaRecta(punto0, punto1){
		
		//pendiente
		this.m = (punto1.y-punto0.y)/(punto1.x-punto0.x);
		this.c = punto0.y-(this.m*punto0.x);
		this.mostrarEcuacion = function(){
		
			var s = "<" + this.m + "x - y + " + this.c + " = 0>";
			return s;
		
		};
		this.contains = function(point){
			
			var contenido = false;
			var resultado = this.m*point.x-point.y+this.c;
			
			if(resultado == 0){contenido = true;}
			
			return contenido;
		
		}
		this.distanceFrom = function(point){
			var d;
			var abs = Math.abs(this.m*point.x-point.y+this.c)
			var raiz = Math.sqrt(Math.pow(this.m,2)+Math.pow(-1,2));
			d=abs/raiz;
			return d;		
		}
		
	}
	//Contains o DistanceFrom
	GPolyline.prototype.pompeia = function(point) {
		var distanciaMinima = 10;
		var existe = false
		var j=0;
		var plocal = new pompeiaPunto(point.lat(),point.lng());
		var cadena = plocal.mostrarPunto();
		//document.getElementById('avisos').innerHTML = cadena;
		
		for(i=0;i < this.getVertexCount()-1;i++){
			j++;
			//var s200 = i+ ", " + j
			//document.getElementById('avisos1').innerHTML = document.getElementById('avisos1').innerHTML + s200 + "<br />";
			var plocal2 = new pompeiaPunto(this.getVertex(i).lat(),this.getVertex(i).lng());
			var plocal3 = new pompeiaPunto(this.getVertex(j).lat(),this.getVertex(j).lng());
			
			var rlocal = new pompeiaRecta(plocal2,plocal3);
			
			var distance = rlocal.distanceFrom(plocal);
			if(distance<10.1e-6 ){existe=true;}
			//document.getElementById('avisos1').innerHTML = document.getElementById('avisos1').innerHTML + distance + "<br />";
			
			//if(rlocal.dis(plocal)){existe = true;}
			
		
		}	
		//document.getElementById('avisos1').innerHTML = document.getElementById('avisos1').innerHTML + distanciaMinima + "<br />";
		return existe;
	}