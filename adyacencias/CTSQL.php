<?



include("../db/conexiondb.php");
include("../master/funciones.php");
$link = Conectarse();


$sql = "SELECT * FROM `calles`";

$resultado = mysql_query($sql,$link);
$num=1;

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1"/>
    <title>Mapa trafico 0.9</title>
    <?
	//funcion en "./master/funciones.php"
	agregarAPIKey();
	?>
	<script type="text/javascript">
    
    //Variables globales
    var dtPolylines = [];
    var dt = '';
    
    function load(){
      
      var dtCalles = [];
      var dtCoordenadas = [];
      var dtDirecciones = [];
      var strCalles = ''; 
      var poly;
      var k = 0;
      
      //data table para las calles, estas calles se obtendran de la base de datos, �que tal hacer un constructor de objetos calles en lugar de tantas variables? Ya te comento mi idea.
      <?
	  while ($calle = mysql_fetch_array($resultado) ){
	  
	  ?>
	  dtCalles[<?=$calle[id_calle]?>-1] = new calle('<?=$calle[id_calle]?>','calle1','<?=$calle[inicio_coordenada]?>', '<?=$calle[fin_coordenada]?>');
      <?
	  }
	  ?>
      
      //Bucle para mostrar las calles a tratar y crear las GDirections correspondientes.
      //for (i=0; i<dtCalles.length; i++){
	  var j=0;
	  k=70;
	  for (i=70; j<7; i++){
	  j++;
	  strCalles += dtCalles[i].id + ' | ' + dtCalles[i].nombre + ' | ' + dtCalles[i].inicio + ' | ' + dtCalles[i].fin + '<br />' ;
        
        dtCoordenadas[i] = 'from: ' + dtCalles[i].inicio + ' to: ' + dtCalles[i].fin;
        
        dtDirecciones[i] = new GDirections();
        GEvent.addListener(dtDirecciones[i], "load", onGDirectionsLoad);
        dtDirecciones[i].load(dtCoordenadas[i], {getPolyline:true});
        document.getElementById('tablaAux').innerHTML = 'Procesando tramos...';
      }   
      
      document.getElementById('tabla').innerHTML = strCalles;
    
      //Cada vez que carguemos un GDirections obtenemos su polilinea y la almacenamos en el data table de polilineas para usarlas luego.
      function onGDirectionsLoad() {
        var poly = dtDirecciones[k].getPolyline();
        poly = dtDirecciones[k].getPolyline();
        dtPolylines [k] = poly;
        k++;
		if (k == dtDirecciones.length)
			alertaNumVertices();
      }
      
      //Esto es para dar tiempo a que se ejectuen todas las instrucciones que generan los polylines correctamente.
      //Creo que he encontrado una manera de adaptar el TimeOut de acuerdo el n�mero de calles por si en un futuro se a�aden muchas m�s calles.
      //setTimeout("alertaNumVertices()", 2000);      
      
    }
    
    //Constructor de la clase calle.
    function calle (id, nombre, inicio, fin){
      this.id = id;
      this.nombre = nombre;
      this.inicio = inicio;
      this.fin = fin;
    }
    
    
    //Esta es la funci�n que obtiene los tramos con sus vertices correspondientes de inicio y fin.
    //No he encontrado al manera de que de algunas coordenadas con menos decimales. No se si tantos decimales te supondran un problema.
    function alertaNumVertices(){
      var aux;
      for (i = 70; i < 77; i++){
        aux = i+1;
        for (j = 0; j < dtPolylines[i].getVertexCount()-1; j++){
          dt += '[ ' + aux + ', ' + (j+1) + ", '" + dtPolylines[i].getVertex(j) + "', '" + dtPolylines[i].getVertex(j+1) + "' ];<br />";
        }
      }
      document.getElementById('tablaAux').innerHTML = dt;
    }

    
    </script>
  </head>
  <body onload="load()" >
    <div id="tabla"></div>
    <br />
    <div id="tablaAux"></div>
  </body>
</html>