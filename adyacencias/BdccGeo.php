	<?	
// Code to find the distance in metres between a lat/lng point and a polyline of lat/lng points
// All in WGS84. Free for any use.
//
// Bill Chadwick 2007
		
		class bdccGeo{
		
			private $x;
			private $y;
			private $z;
			// Construct a bdccGeo from its latitude and longitude in degrees
			//function bdccGeo($lat, $lon)
			public function __construct($lat, $lon) 
			{
				
				$theta = ($lon * pi() / 180.0);
				$rlat = bdccGeoGeocentricLatitude($lat * pi() / 180.0);
				$c = cos($rlat);	
				$this->x = $c * cos($theta);
				$this->y = $c * sin($theta);
				$this->z = sin($rlat);		
			}
			// Convert from geographic to geocentric latitude (radians).
			function bdccGeoGeocentricLatitude($geographicLatitude) 
			{
				$flattening = 1.0 / 298.257223563;//WGS84
				$f = (1.0 - $flattening) * (1.0 - $flattening);
				return atan((tan($geographicLatitude) * $f));
			}
			// Convert from geocentric to geographic latitude (radians)
			function bdccGeoGeographicLatitude ($geocentricLatitude) 
			{
				$flattening = 1.0 / 298.257223563;//WGS84
				$f = (1.0 - $flattening) * (1.0 - $flattening);
				return atan(tan($geocentricLatitude) / $f);
			}
			// Returns the two antipodal points of intersection of two great
			// circles defined by the arcs geo1 to geo2 and
			// geo3 to geo4. Returns a point as a Geo, use .antipode to get the other point
			function bdccGeoGetIntersection( $geo1,  $geo2,  $geo3,  $geo4) 
			{
				$geoCross1 = $geo1->crossNormalize($geo2);
				$geoCross2 = $geo3->crossNormalize($geo4);
				return $geoCross1->crossNormalize($geoCross2);
			}
			
			//from Radians to Meters
			function bdccGeoRadiansToMeters($rad)
			{
				return $rad * 6378137.0; // WGS84 Equatorial Radius in Meters
			}

			//from Meters to Radians
			function bdccGeoMetersToRadians($m)
			{
				return $m / 6378137.0; // WGS84 Equatorial Radius in Meters
			}
			
			// Construct a bdccGeo from its latitude and longitude in degrees
			/*function bdccGeo($lat, $lon) 
			{
				$theta = ($lon * pi() / 180.0);
				$rlat = bdccGeoGeocentricLatitude($lat * pi() / 180.0);
				$c = cos($rlat);	
				$this->x = c * cos($theta);
				$this->y = c * sin($theta);
				$this->z = sin($rlat);		
			}*/
			
			//Properties
			function getLatitudeRadians()
			{
				return (bdccGeoGeographicLatitude(atan2($this->z,sqrt(($this->x * $this->x) + ($this->y * $this->y)))));
			}
			//---------------------------------------------------------
			function getLongitudeRadians()
			{
				return (atan2($this->y, $this->x));
			}
			//---------------------------------------------------------
			function getLatitude()
			{
				return $this->getLatitudeRadians()  * 180.0 / pi();
			}
			//---------------------------------------------------------
			function getLongitude()
			{	
				return $this->getLongitudeRadians()  * 180.0 / pi() ;
			}
			//Maths
			function dot($b)
			{
				return (($this->x * $b->x) + ($this->y * $b->y) + ($this->z * $b->z));
			}
			//---------------------------------------------------------
			function crossLength($b)
			{
				$x = ($this->y * $b->z) - ($this->z * $b->y);
				$y = ($this->z * $b->x) - ($this->x * $b->z);
				$z = ($this->x * $b->y) - ($this->y * $b->x);
				return sqrt(($x * $x) + ($y * $y) + ($z * $z));
			}
			//---------------------------------------------------------
			function scale($s)
			{
				$r = new bdccGeo(0,0);
				$r->x = $this->x * $s;
				$r->y = $this->y * $s;
				$r->z = $this->z * $s;
				return $r;
			}
			//---------------------------------------------------------
			function crossNormalize($b)
			{
				$x = ($this->y * $b->z) - ($this->z * $b->y);
				$y = ($this->z * $b->x) - ($this->x * $b->z);
				$z = ($this->x * $b->y) - ($this->y * $b->x);
				//echo "ra�z cuadrada de (".$x."*".$x.")+(".$y."*".$y.")+(".$z."*".$z.")";
				$L = sqrt(($x * $x) + ($y * $y) + ($z * $z));
				$r = new bdccGeo(0,0);
				if($L ==0){
				}
				else{
				$r->x = $x / $L;
				$r->y = $y / $L;
				$r->z = $z / $L;
				}
				return $r;
			}
			// point on opposite side of the world to this point
			function antipode()
			{
				return $this->scale(-1.0);
			}
			//distance in radians from this point to point v2
			function distance($v2)
			{	
				return atan2($v2->crossLength($this), $v2->dot($this));
			}
			//returns in meters the minimum of the perpendicular distance of this point from the line segment geo1-geo2  !!!!!!!! ESTA ES LA BUENA?
			//and the distance from this point to the line segment ends in geo1 and geo2 
			public function distanceToLineSegMtrs($geo1, $geo2)
			{            
		
				//point on unit sphere above origin and normal to plane of geo1,geo2
				//could be either side of the plane
				$p2 = $geo1->crossNormalize($geo2); 

				// intersection of GC normal to geo1/geo2 passing through p with GC geo1/geo2
				$ip = bdccGeoGetIntersection($geo1,$geo2,$this,$p2); 

				//need to check that ip or its antipode is between p1 and p2
				$d = $geo1->distance($geo2);
				$d1p = $geo1->distance($ip);
				$d2p = $geo2->distance($ip);
				//window.status = d + ", " + d1p + ", " + d2p;
				if (($d >= $d1p) && ($d >= $d2p)) 
					return bdccGeoRadiansToMeters($this->distance($ip));
				else
				{
					$ip = $ip->antipode(); 
					$d1p = $geo1->distance($ip);
					$d2p = $geo2->distance($ip);
				}
				if (($d >= $d1p) && ($d >= $d2p)) 
					return bdccGeoRadiansToMeters($this->distance($ip)); 
				else 
					return bdccGeoRadiansToMeters(min($geo1->distance($this),$geo2->distance($this))); 
			}

		}
		
		
		
		
		//bdccGeo.prototype = new bdccGeo();
		
		// internal helper functions =========================================
		
	    // Convert from geographic to geocentric latitude (radians).
		function bdccGeoGeocentricLatitude($geographicLatitude) 
		{
			$flattening = 1.0 / 298.257223563;//WGS84
		    $f = (1.0 - $flattening) * (1.0 - $flattening);
			return atan((tan($geographicLatitude) * $f));
		}
		
		// Convert from geocentric to geographic latitude (radians)
		function bdccGeoGeographicLatitude ($geocentricLatitude) 
		{
			$flattening = 1.0 / 298.257223563;//WGS84
		    $f = (1.0 - $flattening) * (1.0 - $flattening);
			return atan(tan($geocentricLatitude) / $f);
		}
		
		 // Returns the two antipodal points of intersection of two great
		 // circles defined by the arcs geo1 to geo2 and
		 // geo3 to geo4. Returns a point as a Geo, use .antipode to get the other point
		function bdccGeoGetIntersection( $geo1,  $geo2,  $geo3,  $geo4) 
		{
			$geoCross1 = $geo1->crossNormalize($geo2);
			$geoCross2 = $geo3->crossNormalize($geo4);
			return $geoCross1->crossNormalize($geoCross2);
		}
		
		//from Radians to Meters
		function bdccGeoRadiansToMeters($rad)
		{
			return $rad * 6378137.0; // WGS84 Equatorial Radius in Meters
		}

		//from Meters to Radians
		function bdccGeoMetersToRadians($m)
		{
			return $m / 6378137.0; // WGS84 Equatorial Radius in Meters
		}
		/*
		// properties =================================================
		

		bdccGeo.prototype.getLatitudeRadians = function() 
		
		

		bdccGeo.prototype.getLongitudeRadians = function() 
		
		
		
		bdccGeo.prototype.getLatitude = function() 
		

		bdccGeo.prototype.getLongitude = function() 
		

		// Methods =================================================

        //Maths
		bdccGeo.prototype.dot = function( b) 
		

        //More Maths
		bdccGeo.prototype.crossLength = function( b) 
		
		
	  //More Maths
		bdccGeo.prototype.scale = function( s) 
		

        // More Maths
		bdccGeo.prototype.crossNormalize = function( b) 
		
		
	  // point on opposite side of the world to this point
		bdccGeo.prototype.antipode = function() 
		


        //distance in radians from this point to point v2
		bdccGeo.prototype.distance = function( v2) 
		

	  //returns in meters the minimum of the perpendicular distance of this point from the line segment geo1-geo2
	  //and the distance from this point to the line segment ends in geo1 and geo2 
		bdccGeo.prototype.distanceToLineSegMtrs = function(geo1, geo2)
		
	*/
	/*NO SE HA PASADO A PHPLO SIGUIENTE
        // distance in meters from GLatLng point to GPolyline or GPolygon poly
       


	   function bdccGeoDistanceToPolyMtrs($poly, $point)
        {
            var $d = 999999999;
            var $i;
            var $p = new bdccGeo($point.lat(),$point.lng());
            for($i=0; $i<($poly.getVertexCount()-1); i++)
                 {
                    var $p1 = $poly.getVertex(i);
                    var $l1 = new bdccGeo($p1.lat(),$p1.lng());
                    var $p2 = $poly.getVertex(i+1);
                    var $l2 = new bdccGeo(p2.lat(),p2.lng());
                    var $dp = p.distanceToLineSegMtrs(l1,l2);
                    if(dp < d)
                        d = dp;    
                 }
             return d;
        }

        // get a new GLatLng distanceMeters away on the compass bearing azimuthDegrees
        // from the GLatLng point - accurate to better than 200m in 140km (20m in 14km) in the UK

        function bdccGeoPointAtRangeAndBearing (point, distanceMeters, azimuthDegrees) 
        {
             var latr = point.lat() * Math.PI / 180.0;
             var lonr = point.lng() * Math.PI / 180.0;
     
             var coslat = Math.cos(latr); 
             var sinlat = Math.sin(latr); 
             var az = azimuthDegrees* Math.PI / 180.0;
             var cosaz = Math.cos(az); 
             var sinaz = Math.sin(az); 
             var dr = distanceMeters / 6378137.0; // distance in radians using WGS84 Equatorial Radius
             var sind = Math.sin(dr); 
             var cosd = Math.cos(dr);
      
            return new GLatLng(Math.asin((sinlat * cosd) + (coslat * sind * cosaz)) * 180.0 / Math.PI,
            (Math.atan2((sind * sinaz), (coslat * cosd) - (sinlat * sind * cosaz)) + lonr) * 180.0 / Math.PI); 
        }
	*/
?>