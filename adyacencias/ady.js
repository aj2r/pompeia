
	var map = new GMap2(document.getElementById("map"));
	
	
	
	

	if (GBrowserIsCompatible()) {
	
	
	var routes = [];
	routes[0] = 'from:37.386960, -5.972490 to:37.382925, -5.973281';
	routes[1] = 'from:37.382589, -5.973393 to:37.377244, -5.975202';
	routes[2] = 'from:37.37447,-5.99444 to:37.37154,-5.99739';
	routes[3] = 'from:37.38749,-5.95982 to:37.387352, -5.972358';
	routes[4] = 'from:37.37318,-5.96475 to:37.36984,-5.96903';			//Tamarguillo
	
	var calle;
	
	var c1 = new GDirections(map);
	GEvent.addListener(c1, "load", function() {
		calle = c1.getPolyline();
		calle.show();
		
    });
	c1.load(routes[4], {getPolyline:true});
	
	
	
	map.addControl(new GMapTypeControl());
    map.setCenter(new GLatLng(0,0),2);
    //El trayecto sobre el que se va a buscar las adyacencias
	var dirn = new GDirections();
	//va aumentado de step en step hasta llegar al final(eol)
	var step = 20; // metres
	//tick es el tiempo del setTimeOut de animate
    var tick = 2; // milliseconds
    //Polyline de dirn
	var poly;
    //Distancia total del recorrido
	var eol;
    var car = new GIcon();
		car.image="./cochecito_files/caricon.png"
		car.iconSize=new GSize(32,18);
		car.iconAnchor=new GPoint(16,9);
    var marker;
	//Se usa para ir centrando el mapa con respecto donde estemos cada (180/step) centra el mapa
    var k=0;
	//La GDirections tienen información sobre que hay que hacer en la proxima intersección, stepnum es para saber en que step de la GDirections estamos
    var stepnum=0;
    var speed = "";   
	
	GEvent.addListener(dirn,"load", function() {
		document.getElementById("controls").style.display="none";
		poly=dirn.getPolyline();
		eol=poly.Distance();
		map.setCenter(poly.getVertex(0),17);
		map.addOverlay(new GMarker(poly.getVertex(0),G_START_ICON));
		map.addOverlay(new GMarker(poly.getVertex(poly.getVertexCount()-1),G_END_ICON));
		marker = new GMarker(poly.getVertex(0),{icon:car});
		map.addOverlay(marker);
		var steptext = dirn.getRoute(0).getStep(stepnum).getDescriptionHtml();
		document.getElementById("step").innerHTML = steptext;
		setTimeout("animate(0)",200);  // Allow time for the initial map display
		
		//document.getElementById("avisos").innerHTML = "El primer punto es: " + primerPunto + "<br />El último punto es: " + ultimoPunto;
    });

    GEvent.addListener(dirn,"error", function() {
		alert("Location(s) not recognised. Code: "+dirn.getStatus().code);
    });
	
	function start() {
        var startpoint = document.getElementById("startpoint").value;
        var endpoint = document.getElementById("endpoint").value;
        dirn.loadFromWaypoints([startpoint,endpoint],{getPolyline:true,getSteps:true});
		dirb.loadFromWaypoints(['37.39377,-5.97031','37.3865,-5.97254']);
    }
	
    function animate(d) {
        if (d>eol) {
			document.getElementById("step").innerHTML = "<b>Ruta terminada</b>";
			document.getElementById("distance").innerHTML =  "Metros: "+(d).toFixed(2);
			return;
        }
        var p = poly.GetPointAtDistance(d);
				
        if (k++>=180/step) {
			map.panTo(p);
			k=0;
        }
        marker.setPoint(p);
        document.getElementById("distance").innerHTML =  "Kms: "+(d/1000).toFixed(2)+speed;
		
		//if(bdccGeoDistanceToPolyMtrs(calle, p)<40){
			document.getElementById("avisos2").innerHTML +=  p + " -- " + bdccGeoDistanceToPolyMtrs(calle, p) +  "<br />" ;
		//}
		
		if (stepnum+1 < dirn.getRoute(0).getNumSteps()) {
			if (dirn.getRoute(0).getStep(stepnum).getPolylineIndex() < poly.GetIndexAtDistance(d)) {
				stepnum++;
				var steptext = dirn.getRoute(0).getStep(stepnum).getDescriptionHtml();
				document.getElementById("step").innerHTML = "<b>Next:</b> "+steptext;
				var stepdist = dirn.getRoute(0).getStep(stepnum-1).getDistance().meters;
				var steptime = dirn.getRoute(0).getStep(stepnum-1).getDuration().seconds;
				var stepspeed = ((stepdist/steptime) * 2.24).toFixed(0);
				//step = stepspeed/2.5;
				speed = "<br>Current speed: " + step +" mph"; //step<->stepspeed
			}
        } 
		else {
			if (dirn.getRoute(0).getStep(stepnum).getPolylineIndex() < poly.GetIndexAtDistance(d)) {
				document.getElementById("step").innerHTML = "<b>Next: Arrive at your destination</b>";
				//document.getElementById("avisos2").innerHTML +=  p + ' esta a' + dminimus + ' <br />';
			}
        }
        setTimeout("animate("+(d+step)+")", tick);
    }

}
	
	
  