function pompeiaPunto(x,y){
		
		this.x = x;
		this.y = y;
		this.mostrarPunto = function(){
			return "("+this.x+", "+this.y+")";
		}
	}
	
	
	/***********************
	*Recta que pasa por dos puntos
	* Ax+By+C=0
	************************/
	function pompeiaRecta(punto0, punto1){
		
		//pendiente
		this.m = (punto1.y-punto0.y)/(punto1.x-punto0.x);
		this.c = punto0.y-(this.m*punto0.x);
		this.mostrarEcuacion = function(){
		
			var s = "<" + this.m + "x - y + " + this.c + " = 0>";
			return s;
		
		};
		this.contains = function(point){
			
			var contenido = false;
			var resultado = this.m*point.x-point.y+this.c;
			
			if(resultado == 0){contenido = true;}
			
			return contenido;
		
		}
		this.distanceFrom = function(point){
			var d;
			var abs = Math.abs(this.m*point.x-point.y+this.c)
			var raiz = Math.sqrt(Math.pow(this.m,2)+Math.pow(-1,2));
			d=abs/raiz;
			return d;		
		}
		
	}