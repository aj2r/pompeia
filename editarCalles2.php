<?
session_start();
include_once './db/conexiondb.php';
?>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1"/>
    <title>Editar Calles</title>
	<script type="text/javascript" src="./js/libreriaAjax.js"></script>
	<script src="http://maps.google.com/?file=api&amp;v=2.x&amp;key=ABQIAAAA5-stOrg97YlmemHSV1jIrxSXygBfbFcqNPLe_zgrKCpR9tfcAxRpyJiGx2gJfZQ2gUILVdHnFixS5g" type="text/javascript"></script>
    <script type="text/javascript">
    
    var map;
          
    var routes = [];
		var ruta_alternativa = [];
		    
		var alternativa = new GDirections();
		var ruta = new GDirections();
		
		var listener;

    function load() {

      if (GBrowserIsCompatible()) {
        map = new GMap2(document.getElementById("map"));
        map.addControl(new GSmallMapControl());
        map.addControl(new GMapTypeControl());
        map.setCenter(new GLatLng(37.399074,-5.996475), 13);
        map.enableScrollWheelZoom();
      }       
      
    }
      

    function cargarRuta (inicio, fin, intermedio){

	 document.getElementById('recalcula').innerHTML = inicio+", "+fin+", "+intermedio;
     ruta_alternativa [0] = inicio;
     ruta_alternativa [1] = intermedio;
     ruta_alternativa [2] = fin;
      
     routes [0] = inicio;
     routes [1] = fin;
      
     listener = GEvent.addListener(ruta, "load", onGDirectionsLoad);

     ruta.loadFromWaypoints(routes, {getPolyline:true});
        
     function onGDirectionsLoad() {
     
      var polyline;
      polyline = ruta.getPolyline();
      polyline.setStrokeStyle({color:'blue',weight:3,opacity: 0.7});
      map.addOverlay(polyline);
         
      if (intermedio != ''){
       cargarAlternativa();
      }
     }   
                
    }    
        
      
      

		//=====================================================================================================================
		  
      
    function cargarAlternativa () {
      
      GEvent.removeListener(listener);
      GEvent.addListener(alternativa, "load", dibujaAlternativa);
        
      alternativa.loadFromWaypoints(ruta_alternativa, {getPolyline:true});
    }
      
    function dibujaAlternativa() {
      
      var polyline;
      polyline = alternativa.getPolyline();
      polyline.setStrokeStyle({color:'red',weight:3,opacity: 0.7});
      map.addOverlay(polyline);
          
    }    
		//=====================================================================================================================
      
	function actualizarBD(calle){
	
		var form = document.getElementById("formulario_"+calle);
		var avisos = document.getElementById("avisos_"+calle);
		var nombre = form.name.value;
		var inicio = form.from.value;
		var fin = form.to.value;
		var alternativa = form.alternative.value;
		var s='';
		
		s = s + 'id_calle=' + calle;
		s = s + '&nombre_calle=' + nombre;
		s = s + '&inicio_coordenada=' + inicio;
		s = s + '&fin_coordenada=' + fin;
		s = s + '&alternativa_coordenada=' + alternativa;
		
		FAjax('./calles/editarCalle.php','avisos_'+calle,s,'POST');
	}
      
    </script>
  </head>
  <body onload="load()" >
  <div id="map" style="width: 800px; height: 600px; margin-right:20px;"></div>
  <br />
  
  <?
  $link = Conectarse();
  $sql = "SELECT * FROM `calles`";
  $result = mysql_query($sql, $link);
	if (!$result) {
		error('A database error occurred while checking your login details.');
	}
	while ($calle = mysql_fetch_array($result)) {
  ?>
  <div id="avisos_<? echo $calle[id_calle];?>"></div>
  <div id="div_<? echo $calle[id_calle];?>" style="background-color: #EEEEEE; margin-bottom:10px;">
      <form id="formulario_<? echo $calle[id_calle];?>" action="#" onsubmit="cargarRuta(this.from.value, this.to.value, this.alternative.value); return false">
        <input type="hidden" id="id_calle" name="id_calle" value="<? echo $calle[id_calle];?>" />
		&nbsp;&nbsp;Calle:&nbsp;
        <input type="text" size="30" id="Address" name="name" value="<?echo $calle[nombre_calle];?>" />
		&nbsp;&nbsp;Desde:&nbsp;
        <input type="text" size="18" id="fromAddress" name="from" value="<?=$calle[inicio_coordenada];?>" />
        &nbsp;&nbsp;Hacia:&nbsp;
        <input type="text" size="18" id="toAddress" name="to" value="<?=$calle[fin_coordenada];?>" />
        &nbsp;&nbsp;Punto intermedio:&nbsp;
        <input type="text" size="18" id="alternativeAddres" name="alternative" value="<?=$calle[alternativa_coordenada];?>" />
        <input name="submit" type="submit" value="Obtener ruta!" />
		<input type="button" value="Guardar en BD" onclick="actualizarBD(<? echo $calle[id_calle];?>)" />
      </form>
    </div>
	
   <?
   }
   ?> 
	
	
    <div id="tono"><p>Introducir el punto intermedio mediante coordenadas cartogr&aacute;ficas.</p>Ejemplo de punto intermedio:<br /> 37.3672,-5.98499</div>
    <div id="recalcula"></div>
  </body>
</html>