var map;
var tabla = "<table border='1'>";
var colors = ['red', 'green', 'blue'];
var leidos = 0;
var verticesArray = new Array();

	/*********************************************************
	*
	*
	*********************************************************/
    
	  
	function load() {

		if (GBrowserIsCompatible()) {
			map = new GMap2(document.getElementById("map"));
			map.addControl(new GSmallMapControl());
			map.addControl(new GMapTypeControl());
			map.setCenter(new GLatLng(37.399074,-5.996475), 13);
			map.enableScrollWheelZoom();
			
			
			gdir = new GDirections(map, document.getElementById("directions"));
    
			GEvent.addListener(gdir, "addoverlay", onGDirectionsAddOverlay);
			//GEvent.addListener(gdir, "load", onGDirectionsLoad);

		} 	// if
	      
		tabla = tabla + "</table>";
		document.getElementById('tabla').innerHTML=tabla;
    }	//Load
    
	/*********************************************************
	*
	*
	*********************************************************/
    function leerXml(){
    
		GDownloadUrl("./calles/trafico.xml", function(data, responseCode) {
			var routes = [];
			var estados = [];
			var xml = GXml.parse(data);
			var calles = xml.documentElement.getElementsByTagName("calle");
			var i = 0;
        
			while (i<1 && leidos < calles.length) {
				tabla = tabla + "<tr>";
				var nombre = calles[leidos].getAttribute("nombre");
				tabla = tabla + "<td>" + nombre + "</td>";
				var estado = calles[leidos].getAttribute("estado");
				tabla = tabla + "<td>" + estado + "</td>";
				var inicio_coordenada = calles[leidos].getAttribute("inicio_coordenada");
				tabla = tabla + "<td>" + inicio_coordenada + "</td>";
				var fin_coordenada = calles[leidos].getAttribute("fin_coordenada");
				tabla = tabla + "<td>" + fin_coordenada + "</td>";

				var ruta = 'from: ' + inicio_coordenada + ' to: ' + fin_coordenada;
				routes[i] = ruta;
				estados[i] = estado;
				leidos++;
				i++;

			}	//while     
			
			cargarRutasTrafico(routes, estados);
		});
		
    } 	//leerXML
    
	/*********************************************************
	*
	*
	*********************************************************/	
    function cargarRutasTrafico (routes, estados){
		var directionsArray = [];
		var tono; 
		var k=0;
        for(j = 0; j < routes.length; j++) { 
			directionsArray[j] = new GDirections();
			GEvent.addListener(directionsArray[j], "load", onGDirectionsLoad);
			directionsArray[j].load(routes[j], {getPolyline:true});
        }
		
		
		/*********************************************************
		*
		*
		*********************************************************/
        function onGDirectionsLoad() {
			tono = extraerColor(estados[k]);
			var polyline = directionsArray[k].getPolyline();
			polyline.setStrokeStyle({color:tono,weight:3,opacity: 0.7});
			map.addOverlay(polyline);
			k++;
        }
        
		
		/*********************************************************
		*
		*
		*********************************************************/
        function extraerColor(estado){
            document.getElementById('tono').innerHTML="Calles procesadas: &nbsp;&nbsp;&nbsp;&nbsp;" + leidos + "<br /> Porcentaje de carga: &nbsp;" + parseInt((leidos/77)*100) + " %";

			var color;
			if (estado == "Tr�fico fluido"){
				color = 'green';
			}
			else if (estado == "Tr�fico intenso"){
				color = 'red';
			}
			else if (estado == "Tr�fico muy intenso"){
				color = 'black';
			}
			else{
				color = 'blue';
			}
			return color;
        }
    }
      
      
	/*********************************************************
	*Arrastre de inicio y fin de ruta
	*
	*********************************************************/  
    function setDirections(fromAddress, toAddress, locale) {
		gdir.load("from: " + fromAddress + " to: " + toAddress,{ "locale": locale , "getSteps":true});
		
		
		//s = 'from='+fromAddress;
		//s = s + '&to='+toAddress;
	    //s = s + '&locale='+toAddress;
		
		//FAjax('<?="http://".$_SERVER['SERVER_NAME'];?>/usuario/login2.php','linea_usuario',s,'POST');
    }

      
    /*********************************************************
	*
	*
	*********************************************************/	  
    var newMarkers = [];
    var latLngs = [];
    var icons = [];
	
    //Nota el listener 'addoverlay' GEvent se ha a�adido dentro de la funci�n de inicializaci�n.
	
	/*********************************************************
	*
	*
	*********************************************************/	
    function onGDirectionsAddOverlay(){ 
		// Elimina las anteriores marcas arrastrables. 
        for (var i=0; i<newMarkers.length; i++) {
			map.removeOverlay(newMarkers[i]);
        }

		// Bucle a trav�s de las marcas y creaci�n de las copias arrastrables.
        for (var i=0; i<=gdir.getNumRoutes(); i++) 
        {
			var originalMarker = gdir.getMarker(i);
			latLngs[i] = originalMarker.getLatLng();
			icons[i] = originalMarker.getIcon();
			newMarkers[i] = new GMarker(latLngs[i],{icon:icons[i], draggable:true, title:'Draggable'});
			map.addOverlay(newMarkers[i]);

			// Obtienes los nuevos puntos de ruta en el array y llama a loadFromWaypoints.
			GEvent.addListener(newMarkers[i], "dragend", function(){
        
				var points = [];
				for (var i=0; i<newMarkers.length; i++) 
				{
					points[i]= newMarkers[i].getLatLng();
				}
				gdir.loadFromWaypoints(points);
			});

			//Enlaza el 'click' con la marca oculta original
			copyClick(newMarkers[i],originalMarker);
		
			// oculta o elimina la anterior parca
			//originalMarker.hide();
			map.removeOverlay(originalMarker);
	  
	 
			//A�adido por prosi, para visualizar lnglat del inicio y final de gdir
	  
			var gp = gdir.getPolyline();
			
			mostrarVertices(gp);
			document.getElementById("A").innerHTML = "V�rtice A: " + gp.getVertex(0).toUrlValue();
			document.getElementById("B").innerHTML = "V�rtice B: " + gp.getVertex(gp.getVertexCount()-1).toUrlValue();
	  
		}
		
		/*********************************************************
		*
		*
		*********************************************************/	
		function mostrarVertices(polyline){
			
			var distancia = polyline.Distance();
			
			var d = 0;
			var i = 0;
			var s = "asd";
			while(d<distancia){
				//document.getElementById('panel').innerHTML = d + s + distancia;
				var p = polyline.GetPointAtDistance(d);
				if(i!=0){
					s = s + "&" + i + "=" + p.lat() + ", " + p.lng();
				}
				else{
					s = s + i + "=" + p.lat() + ", " + p.lng();
				}
				d = d+50;
				i++;
		
			}
			FAjax('./adyacencias/index.php','final2',s,'POST');
			//document.getElementById('panel').innerHTML = s;
			
			
			/*var vc = polyline.getVertexCount();
			var s = '1=' + polyline.getVertex(0).toUrlValue();
			document.getElementById("A").innerHTML = "cuento" + vc;
		 
			for(j=1; j<vc; j++){
				s=s+'&' + (j+1) + '=' + polyline.getVertex(j).toUrlValue();
			}
			//document.getElementById("final").innerHTML = s;
			FAjax('./adyacencias/buscaCoincidencias.php','final2',s,'POST');*/
		}
		
		/*********************************************************
		*
		*
		*********************************************************/	
		function copyClick(newMarker,oldMarker){
			GEvent.addListener(newMarker, 'click', function(){
				GEvent.trigger(oldMarker,'click');
			});
		}
    }
		
	
	/*********************************************************
	*Manejo de errores
	*
	*********************************************************/		
    function handleErrors(){
		if (gdir.getStatus().code == G_GEO_UNKNOWN_ADDRESS)
			alert("No se puede encontrar la direcci�n especificada. Direcci�n desconocida,\nError code: " + gdir.getStatus().code);
		else if (gdir.getStatus().code == G_GEO_SERVER_ERROR)
			alert("Las solicitud no pudo ser procesada con �xito. Error en el servidor.\n Error code: " + gdir.getStatus().code);
	    else if (gdir.getStatus().code == G_GEO_MISSING_QUERY)
			alert("Uno de los campos de direcci�n no ha sido especificado.\n Error code: " + gdir.getStatus().code);
		//   else if (gdir.getStatus().code == G_UNAVAILABLE_ADDRESS)  <--- Doc bug... this is either not defined, or Doc is wrong
		//     alert("The geocode for the given address or the route for the given directions query cannot be returned due to legal or contractual reasons.\n Error code: " + gdir.getStatus().code);
	     
		else if (gdir.getStatus().code == G_GEO_BAD_KEY)
			alert("La clave (Google Maps key) introducida no es v�lida. \n Error code: " + gdir.getStatus().code);
		else if (gdir.getStatus().code == G_GEO_BAD_REQUEST)
			alert("La solicitud de direcciones no puede ser procesada.\n Error code: " + gdir.getStatus().code);
		else alert("Ha ocurrido un error desconocido");
	   
    }

      
	/*********************************************************
	*Funcionalidad del Boton Mostrar Tr�fico
	*
	*********************************************************/	
    function cargarEstadoTrafico(){
	  
		var cuatro = 0;
		for (var i = 0; i < 77; i++){
			setTimeout("leerXml()", cuatro);
			cuatro = cuatro + 210; //180 por defecto
		}
	}
	
	/*********************************************************
	* Esta funci�n parece haber quedad obsoleta, parece que hab�a problemas al usar 
	* $_SESSION de php y estas funcionalidades AJAX
	*********************************************************/	
	function conectar(nick, pass){
		s = 'nick='+nick;
		s = s + '&pass='+pass;
	  
		//FAjax('<?="http://".$_SERVER['SERVER_NAME'];?>/usuario/login2.php','linea_usuario',s,'POST');
	}
	  
	/*********************************************************
	* Te lleva a la pagina para cerrar la sesi�n
	*
	*********************************************************/	
	function desconectar(){
		location.href='./cerrarSesion.php';
	}