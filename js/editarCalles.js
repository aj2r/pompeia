/*********************************************************
*
*
*********************************************************/	
var map;
          
var routes = [];
var ruta_alternativa = [];
		    
var alternativa = new GDirections();
var ruta = new GDirections();
		
var listener;

	/*********************************************************
	*
	*
	*********************************************************/

	function load() {

      if (GBrowserIsCompatible()) {
        map = new GMap2(document.getElementById("map"));
        map.addControl(new GSmallMapControl());
        map.addControl(new GMapTypeControl());
        map.setCenter(new GLatLng(37.399074,-5.996475), 13);
        map.enableScrollWheelZoom();
      }       
      
    }
      
	/*********************************************************
	*
	*
	*********************************************************/
    function cargarRuta (inicio, fin, intermedio){

		document.getElementById('recalcula').innerHTML = inicio+", "+fin+", "+intermedio;
		ruta_alternativa [0] = inicio;
		ruta_alternativa [1] = intermedio;
		ruta_alternativa [2] = fin;
      
		routes [0] = inicio;
		routes [1] = fin;
      
		listener = GEvent.addListener(ruta, "load", onGDirectionsLoad);
		//ruta.loadFromWaypoints(routes, {getPolyline:true});
        ruta.load(routes, {getPolyline:true});
		
		function onGDirectionsLoad() {
     
			var polyline;
			polyline = ruta.getPolyline();
			polyline.setStrokeStyle({color:'blue',weight:3,opacity: 0.7});
			map.addOverlay(polyline);
         
			if (intermedio != ''){
				cargarAlternativa();
			}
		}	   
                
    }    
        
      
      

	//=====================================================================================================================
	/*********************************************************
	*
	*
	*********************************************************/	  
      
    function cargarAlternativa () {
      
		GEvent.removeListener(listener);
		GEvent.addListener(alternativa, "load", dibujaAlternativa);
        
		alternativa.loadFromWaypoints(ruta_alternativa, {getPolyline:true});
    }
    /*********************************************************
	*
	*
	*********************************************************/	 
    function dibujaAlternativa() {
      
		var polyline;
		polyline = alternativa.getPolyline();
		polyline.setStrokeStyle({color:'red',weight:3,opacity: 0.7});
		map.addOverlay(polyline);
          
    }    
	//=====================================================================================================================
    /*********************************************************
	* Una vez tenemos en el formulario los datos que queremos podemos pasar a la BD los datos, 
	* -Se usa FAjax, para el Update as�ncrono
	*********************************************************/	   
	function actualizarBD(calle){
	
		var form = document.getElementById("formulario_"+calle);
		var avisos = document.getElementById("avisos_"+calle);
		var nombre = form.name.value;
		var inicio = form.from.value;
		var fin = form.to.value;
		var alternativa = form.alternative.value;
		var s='';
		
		s = s + 'id_calle=' + calle;
		s = s + '&nombre_calle=' + nombre;
		s = s + '&inicio_coordenada=' + inicio;
		s = s + '&fin_coordenada=' + fin;
		s = s + '&alternativa_coordenada=' + alternativa;
		
		FAjax('./calles/editarCalle.php','avisos_'+calle,s,'POST');
	}
     