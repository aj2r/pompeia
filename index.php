<?


/********************************
*-Datos de sesi�n
*-Funciones comunes
*-Funciones para BD
********************************/
include("./master/sesion.php");
include("./master/funciones.php");
include("./db/conexiondb.php");

/**********************************
*-Librerias para parser
*-Parser de trajano para actualizar el XML
**********************************/
include("./master/simple_html_dom.php");
include("./calles/actualizarXML.php");

/**********************************
*-Cabecera de index(intentar aunar conceptos para hacer una misma cabecera
**********************************/
include("./master/arriba.php");

/**********************************
*-Cotenido
**********************************/
include("./index/index_contenido.php");

/**********************************
*-Cierre e etiquetas body-html
************************************/
include("./master/abajo.php");
?>
  