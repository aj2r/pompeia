-- phpMyAdmin SQL Dump
-- version 2.11.4
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generaci�n: 15-10-2008 a las 17:50:02
-- Versi�n del servidor: 5.0.51
-- Versi�n de PHP: 5.2.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Base de datos: `calles_raulete`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calles`
--

CREATE TABLE IF NOT EXISTS `calles` (
  `id_calle` int(3) NOT NULL default '0',
  `nombre_calle` char(50) collate latin1_spanish_ci default NULL,
  `inicio_coordenada` char(70) collate latin1_spanish_ci default NULL,
  `fin_coordenada` char(70) collate latin1_spanish_ci default NULL,
  PRIMARY KEY  (`id_calle`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcar la base de datos para la tabla `calles`
--

INSERT INTO `calles` (`id_calle`, `nombre_calle`, `inicio_coordenada`, `fin_coordenada`) VALUES
(77, 'RONDA NORTE (AV MIRAFLORES-ROTONDA PI CALONGE)', '37.415922,-5.955963', '37.413195,-5.950363'),
(76, 'RONDA NORTE (ROTONDA PI CALONGE-AUTOV�A MADRID)', '37.41268,-5.95032', '37.408278,-5.949354'),
(75, 'RONDA NORTE (AV MIRAFLORES-ROTONDA PI CALONGE)', '37.41587,-5.95609', '37.41295,-5.95034'),
(74, 'RONDA NORTE (CTJO LAS CASILLAS-AV MIRAFLORES)', '37.417345,-5.964975', '37.41612,-5.95684'),
(73, 'RONDA NORTE (AV ALC M VALLE-GTA SAN LAZARO)', '37.41598,-5.97476', '37.41357,-5.98327'),
(72, 'RONDA NORTE (CTJO LAS CASILLAS-AV ALC M VALLE)', '37.4174,-5.9662', '37.41628,-5.97385'),
(71, 'RONDA NORTE (AVDA. MIRAFLORES-CTJO LAS CASILLAS)', '37.41638,-5.9567', '37.41748,-5.96513'),
(70, 'R. PIO XII (MIRAFLORES - MADRESELVA)', '37.40533,-5.97518', '37.40758,-5.97918'),
(69, 'CTRA. CARMONA (K. CITY - R. TAMARGUILLO)', '37.40297,-5.95946', '37.40298,-5.96908'),
(68, 'CTRA. CARMONA (R. TAMARGUILLO - K. CITY)', '37.40284,-5.9691', '37.40286,-5.95955'),
(67, 'CTRA. CARMONA (F. ARI�O - R TAMARGUILLO)', '37.39979,-5.97743', '37.40316,-5.96905'),
(66, 'RONDA TRIANA (M. ARELLANO - C. EXPIRACION)', '37.38575,-6.01125', '37.38986,-6.01028'),
(65, 'LOPEZ DE GOMARA (R. ARGENTINA - SAN JACINTO)', '37.37626,-6.00574', '37.38097,-6.00914'),
(64, 'R. DE CARRANZA (R. TRIANA - ASUNCION )', '37.371424, -6.003821', '37.371420, -5.997913'),
(63, 'CARRERO BLANCO (GTA. CIGARRERAS - ASUNCION)', '37.37448,-5.99448', '37.37148,-5.99739'),
(62, 'S. J. RIVERA (R. HISTORICA - R. PIO XII)', '37.39855,-5.98165', '37.4054,-5.97514'),
(61, 'S. J. RIVERA (R. PIO XII - R. HISTORICA)', '37.40735,-5.97914', '37.39935,-5.98286'),
(60, 'MANUEL SIUROT (C. ILUNDAIN - SU EMINENCIA)', '37.36389,-5.98327', '37.35689,-5.97957'),
(59, 'PUENTE LOS REMEDIOS (GTA. MARINEROS V - ASUNCION)', '37.37549,-5.99183', '37.37472,-5.99752'),
(58, 'E. DATO (BUHAIRA - MENENDEZ PELAYO)', '37.38409,-5.97944', '37.38562,-5.98613'),
(57, 'E. DATO (S. FCO. JAVIER - BUHAIRA)', '37.38293,-5.97328', '37.38412,-5.9793'),
(56, 'RONDA NORTE (ALCA. M VALLE - CTJO LAS CASILLAS)', '37.41601,-5.9735', '37.41709,-5.96532'),
(55, 'RONDA NORTE (ROTONDA PI CALONGE-AVDA MIRAFLORES)', '37.40281,-5.96137', '37.39752,-5.9814'),
(54, 'RONDA NORTE (AUTOVIA MADRID-ROTONDA PI CALONGE)', '37.408410, -5.948260', '37.412850, -5.949910'),
(53, 'MURO DE DEFENSA (BLAS INFANTE-GLTA DESCUBRIMIENTOS', '37.3728,-6.01387', '37.38941,-6.01365'),
(52, 'MURO DE DEFENSA (GLTA DESCUBRIMIENTOS-BLAS INFANTE', '37.38939,-6.01381', '37.37336,-6.0156'),
(51, 'PTE.ALAMILLO (CARLOS III-GLTA OL�MPICA)', '37.41204,-6.00817', '37.41296,-5.98856'),
(50, 'R.NORTE (GLTA OL�MPICA - CARLOS III)', '37.41362,-5.98868', '37.41498,-6.0069'),
(49, 'AVDA. CARLOS III (GLORIETA DESCUBRIMIENTOS-PTE COR', '37.389650, -6.013634', '37.38923,-6.02158'),
(48, 'AVDA. CARLOS III (PTE CORTA-GLORIETA DESCUBRIMIENT', '37.38906,-6.02102', '37.389512, -6.013622'),
(47, 'BUENO MONREAL (AVDA LA PAZ-MANUEL SIUROT)', '37.37467,-5.97622', '37.36812,-5.9854'),
(46, 'D. MNEZ. BARRIOS (RAM�N Y CAJAL-AVDA LA PAZ)', '37.37719,-5.97522', '37.37474,-5.9762'),
(45, 'D. MNEZ. BARRIOS (AVDA PAZ-RAM�N Y CAJAL)', '37.3747,-5.97566', '37.37709,-5.97495'),
(44, 'C. BUENO MONREAL (MANUEL SIUROT-AVDA PAZ)', '37.36812,-5.9854', '37.37427,-5.97592'),
(43, 'TORNEO (PLAZA ARMAS-BARQUETA)', '37.39082,-6.00276', '37.40329,-5.99555'),
(42, 'TORNEO (BARQUETA-PLAZA ARMAS)', '37.4035,-5.99581', '37.39097,-6.00306'),
(41, 'RONDA DEL TAMATGUILLO (KANSAS CITY-AVDA MONTESIERR', '37.39666,-5.96627', '37.39005,-5.95925'),
(40, 'RONDA DEL TAMARGUILLO (AVDA PINO MONTANO-SE-30)', '37.41098,-5.97172', '37.41558,-5.97399'),
(39, 'RONDA TAMARGUILLO (CTRA CARMONA-AVDA PINO MONTANO)', '37.40298,-5.96908', '37.41098,-5.97172'),
(38, 'RONDA TAMARGUILLO (KANSAS CITY-CTRA CARMONA)', '37.397180, -5.966080', '37.40293,-5.96909'),
(37, 'JUAN PABLO II (SE-30-GLORIETA JUAN PABLO II)', '37.37,-6.0131', '37.36749,-5.99781'),
(36, 'AVDA. DE LA PAZ (RONDA TAMARGUILLO-MART�NEZ BARRIO', '37.36958,-5.96941', '37.37483,-5.97573'),
(35, 'AVDA. DE LA PAZ (CTRA SU EMINENCIA-RONDA TAMARGUIL', '37.361273, -5.959567', '37.369209, -5.968957'),
(34, 'AVDA. DE LA PAZ (SE-30-CTRA SU EMINENCIA)', '37.36921,-5.96896', '37.36104,-5.95937'),
(33, 'ARJONA (PTE TRIANA-SAN LAUREANO)', '37.387550, -6.001700', '37.393095, -6.003660'),
(32, 'ARJONA (PZ ARMAS-PTE TRIANA)', '37.390530, -6.002910', '37.387399, -6.001560'),
(31, 'ANDUEZA-DON FADRIQUE (SAN JUAN DE RIBERA-BARQUETA)', '37.403188, -5.988111', '37.403860, -5.995510'),
(30, 'MENENDEZ Y PELAYO (PRADO - LA FLORIDA)', '37.38138,-5.98822', '37.38777,-5.98492'),
(29, 'RONDA TAMARGUILLO (RAMON Y CAJAL - AVDA. DE LA PAZ', '37.373290, -5.964610', '37.369900, -5.968920'),
(28, 'RONDA DEL TAMARGUILLO (MONTESIERRA - LOS ARCOS)', '37.390416, -5.958846', '37.387505, -5.959434'),
(27, 'RONDA TAMARGUILLO (LA OLIVA-AVDA LA PAZ)', '37.37602,-5.97564', '37.37496,-5.97602'),
(26, 'SAN FRANCISCO JAVIER (EDUARDO DATO-RAM�N Y CAJAL)', '37.382589, -5.973393', '37.377244, -5.975202'),
(25, 'LUIS DE MORALES (LUIS MONTOTO - EDUARDO DATO)', '37.386960, -5.972490', '37.382925, -5.973281'),
(24, 'KANSAS CITY (KANSAS CITY-LUIS MONTOTO)', '37.389694, -5.974700', '37.387470, -5.972568'),
(23, 'JOSE LAGUILLO (RONDA HIST�RICA-KANSAS CITY)', '37.390180, -5.975350', '37.394090, -5.983430'),
(22, 'LUIS DE MORALES (EDUARDO DATO - LUIS MONTOTO)', '37.38288,-5.97314', '37.387056, -5.972334'),
(21, 'SAN FRANCISCO JAVIER (RAMON Y CAJAL - EDUARDO DATO', '37.377268, -5.974891', '37.382467, -5.973169'),
(20, 'CRISTO DE LA EXPIRACION (PLAZA ARMAS-GLTA DESCUBRI', '37.39078,-6.00311', '37.3897,-6.01324'),
(19, 'CRISTO DE LA EXPIRACION (GLTA DESCUBRIMIENTOS-PLAZ', '37.38957,-6.01312', '37.39053,-6.00291'),
(18, 'JIMENEZ BECERRIL (BARQUETA - ALAMILLO)', '37.404200, -5.995030', '37.412788, -5.988687'),
(17, 'KANSAS CITY (SANTA JUSTA-RONDA TAMARGUILLO)', '37.390177, -5.974506', '37.396611, -5.966556'),
(16, 'KANSAS CITY (RONDA TAMARGUILLO-SANTA JUSTA)', '37.39681,-5.96657', '37.39035,-5.97459'),
(15, 'KANSAS CITY (CTRA CARMONA-EFESO)', '37.40278,-5.95904', '37.39718,-5.96608'),
(14, 'AVDA. ANDALUCIA (LOS ARCOS - SE30)', '37.387300, -5.958422', '37.386391, -5.941760'),
(13, 'LUIS MONTOTO (CRUZ DEL CAMPO-LOS ARCOS)', '37.38747,-5.96667', '37.38733,-5.95957'),
(12, 'LUIS MONTOTO (LUIS DE MORALES-CRUZ DEL CAMPO)', '37.3872,-5.97225', '37.38747,-5.96667'),
(11, 'LUIS MONTOTO (PUERTA CARMONA-LUIS DE MORALES)', '37.38796,-5.98225', '37.38707,-5.97262'),
(10, 'LUIS MONTOTO (CRUZ DEL CAMPO-LUIS DE MORALES)', '37.38757,-5.9665', '37.387352, -5.972358'),
(9, 'LUIS MONTOTO (LOS ARCOS-CRUZ DEL CAMPO)', '37.387481, -5.959718', '37.38757,-5.9665'),
(8, 'AVDA. ANDALUCIA (SE30-LOS ARCOS)', '37.386520, -5.941718', '37.387450, -5.958760'),
(7, 'RAMON Y CAJAL (SAN FRANCISCO JAVIER-RONDA TAMARGUI', '37.37711,-5.9749', '37.37357,-5.96472'),
(6, 'RAMON Y CAJAL (SAN FRANCISCO JAVIER-AVDA BORBOLLA)', '37.377252, -5.975445', '37.379582, -5.982365'),
(5, 'RAMON Y CAJAL (RONDA TAMARGUILLO-SAN FRANCISCO JAV', '37.373640, -5.964830', '37.377067, -5.974751'),
(4, 'AVDA PALMERA (BONANZA - P�EZ DE RIBERA)', '37.36944,-5.98792', '37.35998, -5.98228'),
(3, 'AVDA. PALMERA (GLTA MARINEROS VOLUNTARIOS-BONANZA)', '37.375096, -5.991428', '37.36944,-5.98792'),
(2, 'AVD PALMERA (GLORIETA PLUS ULTRA-P�EZ DE RIBERA)', '37.356330, -5.980112', '37.35998, -5.98228'),
(1, 'AVDA. PALMERA (SE30-GLORIETA PLUS ULTRA)', '37.342299, -5.973183', '37.355653, -5.979737');
