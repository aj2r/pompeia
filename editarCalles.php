<?

/********************************
*-Datos de sesi�n
*-Funciones comunes
+-Funciones para BD
********************************/
include_once ("./master/sesion.php");
include_once ("./master/funciones.php");
include_once ("./db/conexiondb.php");

/**********************************
*-Cabecera de arribaEditar
**********************************/
include_once ("./master/arribaEditar.php");


//Para poder editar, necesita permisos especiales
if($_SESSION['permisos_usuario']>=10){
	/*********************************
	*Contenido de Editar Calles
	**********************************/
	include("./calles/editarCalles.php");
}
else{
	/*********************************
	*Contenido de Editar Calles
	**********************************/
	include("./NoPermisos.php");
}

/**********************************
*-Cierre e etiquetas body-html
************************************/
include_once ("./master/abajo.php");
  

?>